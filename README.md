# fessor
bot designed for the matematikfessor.dk discord server.

# features
- viggo scanner
- simple prompt/response commands
- permission system
- slander gaming
- moderation features
- schedule manager
- help command

## coming soon
- music player
- gif scanner
