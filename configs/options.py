settings = ['lock', 'funnymode']
values = ['on', 'off']
skemaer = {
  "mandag": "https://i.imgur.com/dWer1b7.png",
  "tirsdag": "https://i.imgur.com/f98WCF8.png",
  "onsdag": "https://i.imgur.com/46vs948.png",
  "torsdag": "https://i.imgur.com/kEhraci.png",
  "fredag": "https://i.imgur.com/C3p0iY6.png"
}
schedules = {
  "0": "https://i.imgur.com/dWer1b7.png",
  "1": "https://i.imgur.com/f98WCF8.png",
  "2": "https://i.imgur.com/46vs948.png",
  "3": "https://i.imgur.com/kEhraci.png",
  "4": "https://i.imgur.com/C3p0iY6.png"
}
commands = {
  "slander": "Slander en member af serveren. Skriv `.s help` for mere info",
  "suggest": "Forslag en ny feature. Dette bliver logget i bottens filer og DM'et til polska.",
  "skema": "Vis dagens skema. Syntaks: `.skema [dag]`",
  "scan": "Scan viggo for lektier. Ingen syntax lige nu.",
  "settings": "Admin only indstillinger. Brug `.settings help` for mere info.",
  "bury": "Begrav chatten med intethed, så chatten bliver \"clearet\".",
  "blacklist": "Add eller fjern tekst fra blacklisten. Admin only.",
  "poggies": "Puha.",
  "badass": "Simons sejhed",
  "perms": "Se eller ændr en brugers tilladelser. Admin only.",
  "next": "Vis, hvornår næste time er.",
  "hjælp": "Denne kommando. LEL",
  "kick": "kick en bruger",
  "ban": "ban en bruger",
  "unban": "unban en bruger",
  "mute": "mute en bruger",
  "unmute": "unmute en bruger"
}